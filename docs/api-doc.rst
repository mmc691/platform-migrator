=========================
Source Code Documentation
=========================

Main module
-----------

.. automodule:: platform_migrator.main
   :members:


Server module
-------------

.. automodule:: platform_migrator.server
   :members:


Migrator module
---------------

.. automodule:: platform_migrator.migrator
   :members:

Package manager module
----------------------

.. automodule:: platform_migrator.package_manager
   :members:

Base system script
------------------

.. automodule:: platform_migrator.base_sys_script
   :members:

Unpack script
-------------

.. automodule:: platform_migrator.unpack
   :members:
