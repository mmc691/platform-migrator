==============================
Platform Migrator Introduction
==============================

Platform migrator is a tool which helps with migrating software from system to
another. Its main aim is to ease the process of installing dependencies
required for the software. It interfaces with various package managers to
install the required dependencies and runs tests to identify whether the
installation was successful or not.

For platform migrator, a system is defined by a set of package managers
provided as part of a configuration file. This set may include some or all of
the package managers available on the machine. In case only some package
managers are used, the system is defined only as the set of those package
managers. This can be used to migrate dependencies of a software from one
package manager to another, on the same machine.

The two systems could also be different OSes or running in a VM. As long as
there is a package manager available, platform migrator can try to port
software across the systems. The rest of the documentation uses the terms base
system and target system to refer to these systems:

base system
    This is the system on which the software is currently installed within a
    conda environment. The current version of platform migrator requires that
    there is a conda installation which can provide the list of packages used
    by the software. The software itself also must be available in an
    executable form, either as a compiled object or as source code.

target system
    This is the system to which the software will be migrated. Platform
    migrator should be installed on this system. Conda is not required on this
    system, but there must be some package manager present that the user can
    use to install software.

The base system and the target system must be able to connect with each other
over a network through HTTP or the user of the base system must also install
platform migrator and email the zip file produced over to the target system.

.. _install:

Installation
------------

Platform migrator requires Python >= 3.5 to be available on the target system.
It can be installed directly from pip on the target system ::

    pip install --user platform-migrator

Or, if you want to install a development version ::

    pip install --user git+https://gitlab.com/mmc691/platform-migrator.git

Note:
    On some systems, you may need to specify ``pip3`` instead of ``pip`` to
    make sure that Python 3 is used. Run ``pip --version`` to check which
    version of Python the default pip installation uses.

If pip is not available, it can be installed by cloning the git repository or
downloading a zip file from `Gitlab repository <https://gitlab.com/mmc691/platform-migrator>`_
and executing ::

    git clone https://gitlab.com/mmc691/platform-migrator
    cd platform-migrator
    python3 setup.py install --user

Execution
---------

This provides a *basic* overview of how platform migrator works. See the
tutorials and the internals documentation for the full details on how to use
platform migrator.

The whole process is executed in 4 main steps:

1. Start the server on the target system with ``platform-migrator server start``.
   This starts an HTTP server which by default listens on localhost:9001.

2. On the base system, execute the following on the command line: ::

       curl http://<server-name>:<server-port>/migrate > script.py
       python script.py

   The server name and port are the hostname and port on which the server on
   the target system is listening. See :py:mod:`~platform_migrator.base_sys_script`
   documentation for details on what the script does.

3. Generate the test configuration file on the target system. See the
   :doc:`test-config-file` section on how to do this.

4. Run ``platform-migrator migrate <name> <config>`` on the target system.
   See the :doc:`tutorial` and :doc:`case-studies` section for detailed description
   about this step.

Alternately, if a network connection cannot be setup between the systems, the
software can be transported over email as a zip file. In this case, platform
migrator must be installed on the base system as well as the target system.
The steps will be:

1. Package the software on the base system using ``platform-migrator pack``
   and send the zip file created by platform migrator to the target system.

2. Unpack the zip file on the target system using
   ``platform-migrator unpack <zip-file>``.

3. Generate the test configuration file on the target system. See the
   :doc:`test-config-file` section on how to do this.

4. Run ``platform-migrator migrate <name> <config>`` on the target system.
   See the :doc:`tutorial` and :doc:`case-studies` section for detailed description
   about this step.

If the migration was successful, the software will be saved in the configured
output directory.

Supported Package Managers
--------------------------

Out of the box, platform migrator supports conda as a package manager on any
OS. If you wish to use platform migrator only with conda, you can skip this
section. For Linux distros, pip, pacman, apt-get and aptitude are also
supported out of the box. For Mac OS, pip is supported out of the box. For
Windows, pip is supported in a POSIX shell environment like Cygwin.

In case of pip, pip2 and pip3 options are provided as well to explicitly use
pip for a specific Python version. Using pip as the package manager will
default to whichever version of pip is installed as the default. The other
version of pip must be installed before it can be used.

However, platform migrator allows you to configure your own package manager if
you do not wish to use any of the package managers listed above. See
:doc:`package-manager-config-file` and :ref:`config_pkg_mgr` for complete
instructions for that.

Git repo and other links
------------------------

| **Git repository:** https://gitlab.com/mmc691/platform-migrator
| **Online Documentation:** https://platform-migrator.readthedocs.io

In case of bugs or questions, please create an issue through
`Gitlab. <https://gitlab.com/mmc691/platform-migrator/issues>`_
