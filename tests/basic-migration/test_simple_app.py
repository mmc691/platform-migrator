"""Test for conda to conda migration of simple application and no vm"""


import os
import shutil
import subprocess
import sys
import time
import unittest


HOST = 'localhost'
PORT = 9001
HOME = os.getenv('HOME')
CONDA_ROOT_DIR = os.getenv('CONDA_ROOT_DIR') + '/bin'


class Test_simple_app(unittest.TestCase):

    def setUp(self):
        os.chdir(os.path.dirname(os.path.abspath(__file__)))
        self.work_dir = os.getcwd()
        proc0 = subprocess.run([sys.executable, '../../platform_migrator/main.py', 'server',
                                '--host', HOST, '--port', str(PORT), 'start'])
        time.sleep(2)


    def tearDown(self):
        os.chdir(self.work_dir)
        if os.path.isdir('/tmp/simple-app'):
            shutil.rmtree('/tmp/simple-app')
        if os.path.isdir(HOME + '/.platform_migrator/simple-app'):
            shutil.rmtree(HOME + '/.platform_migrator/simple-app')
        if os.path.isdir(HOME + '/.platform_migrator/simple-app-pacman'):
            shutil.rmtree(HOME + '/.platform_migrator/simple-app-pacman')
        if os.path.exists('base_sys_script.py'):
            os.remove('base_sys_script.py')
        subprocess.run([sys.executable, '../../platform_migrator/main.py', 'server', 'stop'])
        subprocess.run('{0}/conda env remove -y -p {0}/../envs/simple-app'.format(
                       CONDA_ROOT_DIR if CONDA_ROOT_DIR else os.getcwd()),
                       stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)

    def test_conda(self):
        proc1 = subprocess.run('curl -s http://%s:%d/migrate' % (HOST, PORT), shell=True,
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.assertEqual(proc1.returncode, 0, proc1.stdout.decode('utf-8'))
        with open('base_sys_script.py', 'w+') as f_obj:
            f_obj.write(proc1.stdout.decode('utf-8'))

        os.chdir(CONDA_ROOT_DIR if CONDA_ROOT_DIR else self.work_dir)
        proc2 = subprocess.run('python %s/base_sys_script.py' % self.work_dir, shell=True,
                               input=(b'root\nsimple-app\n%s/simple-app'
                                      % self.work_dir.encode('utf-8')),
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.assertEqual(proc2.returncode, 0, proc2.stdout.decode('utf-8'))
        os.chdir(self.work_dir)

        self.assertTrue(os.path.isdir(HOME + '/.platform_migrator/simple-app'))
        proc3 = subprocess.run([sys.executable, '../../platform_migrator/main.py', 'migrate',
                                'simple-app', 'test-config.ini'],
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.assertEqual(proc3.returncode, 0, proc3.stdout.decode('utf-8'))
        self.assertTrue(os.path.isdir('/tmp/simple-app'), proc3.stdout.decode('utf-8'))

    def test_pacman(self):
        proc1 = subprocess.run('curl -s http://%s:%d/migrate' % (HOST, PORT), shell=True,
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.assertEqual(proc1.returncode, 0, proc1.stdout.decode('utf-8'))
        with open('base_sys_script.py', 'w+') as f_obj:
            f_obj.write(proc1.stdout.decode('utf-8'))

        os.chdir(CONDA_ROOT_DIR if CONDA_ROOT_DIR else self.work_dir)
        proc2 = subprocess.run('python %s/base_sys_script.py' % self.work_dir, shell=True,
                               input=(b'root\nsimple-app-pacman\n%s/simple-app'
                                      % self.work_dir.encode('utf-8')),
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.assertEqual(proc2.returncode, 0, proc2.stdout.decode('utf-8'))
        os.chdir(self.work_dir)

        self.assertTrue(os.path.isdir(HOME + '/.platform_migrator/simple-app-pacman'))
        proc3 = subprocess.run("yes 0 | %s ../../platform_migrator/main.py migrate \
                               simple-app-pacman test-config.ini" % sys.executable,
                               shell=True,
                               stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        self.assertEqual(proc3.returncode, 0, proc3.stdout.decode('utf-8'))
        self.assertTrue(os.path.isdir('/tmp/simple-app'), proc3.stdout.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
