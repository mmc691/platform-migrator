"""Tests for pck_mgr module and PackageManager class

pip and pip-root are used for testing.

NOTE: Install commands are not executed, only checked for validity
"""

import os
import sys
import unittest

os.chdir(os.path.dirname(os.path.abspath(__file__)) + '/../')
sys.path.insert(0, os.getcwd())

from platform_migrator import package_manager

class Test_get_package_manager(unittest.TestCase):

    def test_vanilla(self):
        self.assertIsNotNone(package_manager.get_package_manager('pip'))

    def test_extra_config_files(self):
        self.assertIsNotNone(package_manager.get_package_manager('pip',
                             extra_config_files=['conf/package-managers.ini']))

    def test_fail(self):
        with self.assertRaises(KeyError):
            package_manager.get_package_manager('foo')


class TestPackageManager_pip(unittest.TestCase):

    def setUp(self):
        self.pck_mgr = package_manager.get_package_manager('pip')

    def test_search(self):
        pcks = self.pck_mgr.search('jedi')
        found_jedi = False
        for pck in pcks:
            self.assertIn('pck', pck)
            self.assertIn('ver', pck)
            self.assertNotEqual(pck['pck'], '')
            self.assertNotEqual(pck['ver'], '')
            found_jedi = found_jedi or pck['pck'] == 'jedi'
        self.assertTrue(found_jedi)

    def test_install(self):
        install_cmd = self.pck_mgr.install('jedi', dry_run=True)
        self.assertEqual(install_cmd, 'pip install --user jedi')

    def test_install_ver(self):
        install_cmd = self.pck_mgr.install('jedi', version='0.10.0', dry_run=True)
        self.assertEqual(install_cmd, 'pip install --user jedi==0.10.0')

class TestPackageManager_pip_root(unittest.TestCase):

    def setUp(self):
        self.pck_mgr = package_manager.get_package_manager('pip-root')

    def test_search(self):
        pcks = self.pck_mgr.search('jedi')
        found_jedi = False
        for pck in pcks:
            self.assertIn('pck', pck)
            self.assertIn('ver', pck)
            self.assertNotEqual(pck['pck'], '')
            self.assertNotEqual(pck['ver'], '')
            found_jedi = found_jedi or pck['pck'] == 'jedi'
        self.assertTrue(found_jedi)

    def test_install(self):
        install_cmd = self.pck_mgr.install('jedi', dry_run=True)
        self.assertEqual(install_cmd, 'sudo pip install jedi')

    def test_install_ver(self):
        install_cmd = self.pck_mgr.install('jedi', version='0.10.0', dry_run=True)
        self.assertEqual(install_cmd, 'sudo pip install jedi==0.10.0')

if __name__ == '__main__':
    unittest.main(exit=False)
