#!/bin/bash

cd $(dirname $0)/../
start_dir=$(pwd)

function cleanup {
    cd /tmp
    . $CONDA_ROOT_DIR/bin/deactivate
    rm -rf base_sys_script.py foo/
    cd $start_dir
    python platform_migrator/main.py server stop
}

trap cleanup EXIT

cd $start_dir
python platform_migrator/main.py server start

cd /tmp
. $CONDA_ROOT_DIR/bin/activate
mkdir foo

curl -s http://localhost:9001/migrate > base_sys_script.py
python base_sys_script.py
